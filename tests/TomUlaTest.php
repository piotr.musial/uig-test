<?php

/*
 * (c) Piotr Musiał <piotr.musial@codehat.pl>
 */

namespace Tests;

use App\Tomula\TomUla;
use PHPUnit\Framework\TestCase;

class TomUlaTest extends TestCase
{
    public function testPrintList()
    {
        ob_start();
        TomUla::printList();
        $result = ob_get_clean();

        $this->assertContains("1\n2\nTom\n4\nUla\nTom", $result);
    }
}
