FROM php:latest

COPY . /usr/src/myapp
WORKDIR /usr/src/myapp

RUN apt-get update -yqq && \
    apt-get install -yqq zip git && \
    pecl install xdebug && \
    docker-php-ext-enable xdebug && \
    curl -sS https://getcomposer.org/installer | php