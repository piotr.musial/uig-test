<?php
declare(strict_types=1);

/*
 * (c) Piotr Musiał <piotr.musial@codehat.pl>
 */

namespace App\Queue;

class Queue_Object implements \Serializable
{
    private $taskName;
    private $priority;

    const MIN_PRIORITY = 0;
    const MAX_PRIORITY = 10;

    public function __construct(string $taskName, int $priority)
    {
        $this->taskName = $taskName;
        $this->setPriority($priority);
    }

    public function setPriority(int $priority): void
    {
        if ($priority < self::MIN_PRIORITY) {
            $this->priority = self::MIN_PRIORITY;
        } elseif ($priority > self::MAX_PRIORITY) {
            $this->priority = self::MAX_PRIORITY;
        } else {
            $this->priority = $priority;
        }
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function getTaskName(): string
    {
        return $this->taskName;
    }

    public function printDescription(): void
    {
        printf("Name: %s Priority: %d \n", $this->getTaskName(), $this->getPriority());
    }

    public function serialize(): string
    {
        return serialize([$this->getTaskName(), $this->getPriority()]);
    }

    public function unserialize($serialized): void
    {
        $data = unserialize($serialized);
        $this->taskName = $data[0];
        $this->setPriority($data[1]);
    }
}
