<?php
declare(strict_types=1);

/*
 * (c) Piotr Musiał <piotr.musial@codehat.pl>
 */

namespace App\Queue;

class Queue implements \Iterator, \Countable, \Serializable
{
    private $currentKey = 0;

    private $items = [];

    public function add(Queue_Object $item): void
    {
        $this->items[] = $item;
        $this->sortItems();
    }

    public function compare(Queue_Object $a, Queue_Object $b): int
    {
        if ($a->getPriority() === $b->getPriority()) {
            return 0;
        }

        return ($a->getPriority() < $b->getPriority()) ? 1 : -1;
    }

    private function sortItems(): void
    {
        uasort($this->items, [$this, 'compare']);
        $this->items = array_values($this->items);
    }

    public function rewind(): void
    {
        reset($this->items);
    }

    public function current(): Queue_Object
    {
        return current($this->items);
    }

    public function key(): string
    {
        return key($this->items);
    }

    public function next(): void
    {
        next($this->items);
    }

    public function valid(): bool
    {
        return key($this->items) !== null;
    }

    public function count(): int
    {
        return count($this->items);
    }

    public function toArray(): array
    {
        return $this->items;
    }

    /**
     * @param $paramName
     * @return int
     * @throws \Exception
     */
    private function getKeyFromParamName($paramName): int
    {
        if (substr($paramName, 0, 4) === "elem") {
            $key = (int)substr($paramName, 4);

            return $key;
        }

        throw new \Exception("Wrong parameter");
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name): Queue_Object
    {
        $key = $this->getKeyFromParamName($name);
        if ($this->keyExist($key)) {
            return $this->getItem($key);
        }

        throw new \Exception("Elem $key not exist");
    }

    /**
     * @param $name
     * @return bool
     * @throws \Exception
     */
    public function __isset($name): bool
    {
        $key = $this->getKeyFromParamName($name);

        return $this->keyExist($key);
    }

    /**
     * @param $name
     * @throws \Exception
     */
    public function __unset($name): void
    {
        $key = $this->getKeyFromParamName($name);
        if (!$this->keyExist($key)) {
            throw new \Exception("Elem $key not exist");
        }
        $this->deleteItem($key);
    }

    public function keyExist($key): bool
    {
        return isset($this->items[$key - 1]);
    }

    public function getItem($key): Queue_Object
    {
        return $this->items[$key - 1];
    }

    public function deleteItem($key): void
    {
        if ($this->currentKey === $key) {
            $this->next();
        }
        unset($this->items[$key - 1]);
        $this->sortItems();
    }

    public function __call($name, $arguments)
    {
        if (!method_exists($this, $name)) {
            foreach ($this as $item) {
                call_user_func_array([$item, $name], $arguments);
            }
        }
    }

    /**
     * @param $name
     * @param $value
     * @throws \Exception
     */
    public function __set($name, $value): void
    {
        $key = $this->getKeyFromParamName($name);
        if (!$this->keyExist($key)) {
            throw new \Exception("Unsupported");
        }
        $this->items[$key - 1] = $value;
        $this->sortItems();
    }

    public function serialize(): string
    {
        return serialize($this->items);
    }

    public function unserialize($serialized): void
    {
        $this->items  = unserialize($serialized);
    }
}
